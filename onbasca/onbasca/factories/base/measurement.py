# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause
"""
Generated using factory_generator 1.0.4.
"""

import factory
from django.utils import timezone

from onbasca.onbasca.models.measurement import Measurement


class MeasurementFactoryBase(factory.django.DjangoModelFactory):
    consensus = factory.SubFactory(
        factory="onbasca.onbasca.factories.ConsensusFactory"
    )
    queued_at = factory.Faker(
        provider="date_time_between",
        start_date="-27d",
        end_date="now",
        tzinfo=timezone.get_current_timezone(),
    )
    attempted_at = factory.Faker(
        provider="date_time_between",
        start_date="-27d",
        end_date="now",
        tzinfo=timezone.get_current_timezone(),
    )
    finished_at = factory.Faker(
        provider="date_time_between",
        start_date="-27d",
        end_date="now",
        tzinfo=timezone.get_current_timezone(),
    )
    relay = factory.SubFactory(
        factory="onbasca.onbasca.factories.RelayFactory"
    )
    webserver = factory.SubFactory(
        factory="onbasca.onbasca.factories.WebServerFactory"
    )
    helper = factory.SubFactory(
        factory="onbasca.onbasca.factories.RelayFactory"
    )
    as_exit = factory.Faker(provider="random_element", elements=(True, False))
    bandwidth = factory.Faker(provider="random_int", min=0, max=2147483647)
    error = factory.Faker(provider="text")

    class Meta:
        model = Measurement

# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

from django.contrib import admin

from onbasca.onbasca.models.bwfile import BwFile


@admin.register(BwFile)
class BwFileAdmin(admin.ModelAdmin):
    # pass
    list_display = (
        "file_created",
        "consensus",
        # "scanner_country",
        # 'destinations_countries',
        # "version",
        # "software",
        # "software_version",
        "_mu",
        "_muf",
        "_bw_sum",
        "_bw_scaled_sum",
        "_limit",
        "_bw_scaled_limited_sum",
        "_bw_scaled_limited_rounded_sum",
        "consensus_routerstatuses_count",
        "relaybw_set_vote_count",
    )
    list_filter = [
        "consensus",
        "file_created",
    ]

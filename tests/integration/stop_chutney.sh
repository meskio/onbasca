#!/bin/bash

# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: CC0-1.0

set -x

CURRENT_DIR=`pwd`
CHUTNEY_DIR=${1:-./chutney}
cd $CHUTNEY_DIR
# Stop chutney network if it is already running
./chutney stop networks/bwscanner
cd $CURRENT_DIR

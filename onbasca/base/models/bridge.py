# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause
"""Bridge base model."""
import logging

from django.db import models

from . import BaseModel

logger = logging.getLogger(__name__)


def fingerprint_from_bridgeline(bridgeline):
    """
    Obtain the bridge fingerprint from a bridge line.

    From ``7. Displaying Bridge Information``
    (https://gitlab.torproject.org/tpo/core/torspec/-/blob/main/bridgedb-spec.txt#L357)
    Bridges are formatted as::

        <address:port> NL

    Pluggable transports are formatted as::

        <transportname> SP <address:port> [SP arglist] NL

    Example::
        - with transport::
          obfs4 <ip>:<port> <fingerprint> cert=<cert> iat-mode=<iat-mode>
        - without transport::
          <ip>:<port> <fingerprint>

    """
    split = bridgeline.split(" ")
    # Assuming only bridgelines with transport are valid
    if len(split) < 5:
        return None
    # and the first is the transport
    if split[0] != "obfs4":
        return None
    # not checking ip/port nor fingerprint
    return split[2]


class BridgeManagerBase(models.Manager):
    def from_bridgeline(self, bridgeline):
        """Create a bridge from a bridgeline"""
        logger.debug("Creating bridge from bridgeline %s", bridgeline)
        fingerprint = fingerprint_from_bridgeline(bridgeline)
        if not fingerprint:
            return "bridgeline is not valid.", False
        bridge, created = self.update_or_create(
            fingerprint=fingerprint,
            defaults={"bridgeline": bridgeline},
        )
        logger.debug("Bridge %s created %s", bridge, created)
        return bridge, created


class BridgeBase(BaseModel):
    class Meta:
        abstract = True

    objects = BridgeManagerBase()
    fingerprint = models.CharField(primary_key=True, max_length=40)
    # What's the max and min length?, 154?
    bridgeline = models.CharField(max_length=255, null=True, blank=True)

    def __str__(self):
        return "{}".format(self.fingerprint)

# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause
import json
import pytest
from django.test import Client

from onbasca.base.models import bridge
from onbrisca import config

@pytest.mark.unit_test
@pytest.mark.django_db
def test_create_bridge():
    # Client converts the data past as dictionary into URL-encoded form.
    c = Client()
    # Valid bridge_line
    bridge_line = "obfs4 0.0.0.0:00000 AAA cert=0 iat-mode=0"

    # With the test.Client it's needed to pass `/` before the path
    # Successful request
    response = c.get(
        "/" + config.API_PATH, {"bridge_lines": [bridge_line]}
    )
    assert 200 == response.status_code
    response_dict = json.loads(response.content)
    bridge_results = response_dict["bridge_results"]
    bridge_result = bridge_results[bridge_line]
    assert bridge_result["functional"] is True
    assert bridge_result.get("error", None) is None

    # Do the same request
    response = c.get(
        "/" + config.API_PATH, {"bridge_lines": [bridge_line]}
    )
    assert 200 == response.status_code
    response_dict = json.loads(response.content)
    bridge_results = response_dict["bridge_results"]
    bridge_result = bridge_results[bridge_line]
    assert bridge_result["functional"] is True
    assert bridge_result.get("error", None) is None

    # Do the same request, bridge_lines is not a list.
    # The client "magically" converts it to a dictionary, but it's not the
    # case of a real request, which would fail anyway cause it's not json.
    response = c.get(
        "/" + config.API_PATH, {"bridge_lines": bridge_line}
    )
    assert 200 == response.status_code
    response_dict = json.loads(response.content)
    bridge_results = response_dict["bridge_results"]
    bridge_result = bridge_results[bridge_line]
    assert bridge_result["functional"] is True
    assert bridge_result.get("error", None) is None

    # Bridgeline with fingerprint only
    bridge_line = "AAA"
    response = c.get("/" + config.API_PATH, {"bridge_lines": [bridge_line]})
    assert 200 == response.status_code
    response_dict = json.loads(response.content)
    bridge_results = response_dict["bridge_results"]
    bridge_result = bridge_results[bridge_line]
    assert bridge_result["functional"] is False
    assert bridge_result.get("error", None) == "bridgeline is not valid."

    # No bridge_lines
    bridge_line = "obfs4 0.0.0.0:00000 AAA cert=0 iat-mode=0"
    response = c.get("/" + config.API_PATH, {"line": [bridge_line]})
    assert 400 == response.status_code
    assert (
        b'{"bridge_results": [], "time": 0}' == response.content
    )

    # Empty bridge_lines
    response = c.get("/" + config.API_PATH, {"bridge_lines": []})
    assert 400 == response.status_code
    assert b'{"bridge_results": [], "time": 0}' == response.content

    # Content-type `application/x-www-form-urlencoded` XXXX
    response = c.get(
        "/" + config.API_PATH, {"bridge_lines": [bridge_line]},
        content_type="application/x-www-form-urlencoded"
    )
    assert 200 == response.status_code
    response_dict = json.loads(response.content)
    bridge_results = response_dict["bridge_results"]
    bridge_result = bridge_results[bridge_line]
    assert bridge_result["functional"] is True
    assert bridge_result.get("error", None) is None

    # Content-type `application/json`
    response = c.get(
        "/" + config.API_PATH, {"bridge_lines": [bridge_line]},
        content_type="application/json"
    )
    assert 200 == response.status_code
    response_dict = json.loads(response.content)
    bridge_results = response_dict["bridge_results"]
    bridge_result = bridge_results[bridge_line]
    assert bridge_result["functional"] is True
    assert bridge_result.get("error", None) is None

    # POST request
    response = c.post(
        "/" + config.API_PATH, {"bridge_lines": [bridge_line]}
    )
    assert 403 == response.status_code
    assert b'{"bridge_results": [], "time": 0}' == response.content

    # Wrong path
    response = c.get("bridge/", {"bridge_lines": [bridge_line]},)
    assert 404 == response.status_code
    assert (
        b'\n<!doctype html>\n<html lang="en">\n<head>\n  <title>Not Found</title>\n</head>\n<body>\n  <h1>Not Found</h1><p>The requested resource was not found on this server.</p>\n</body>\n</html>\n'  # noqa:E501
        == response.content
    )

    # Several bridge_lines
    response = c.get(
        "/" + config.API_PATH,
        {"bridge_lines":
            [
                bridge_line,
                bridge_line,
                "obfs4 0.0.0.0:00000 BBB cert=0 iat-mode=0",
            ]
        },
        content_type="application/json"
    )
    assert 200 == response.status_code
    response_dict = json.loads(response.content)
    bridge_results = response_dict["bridge_results"]
    bridge_result = bridge_results[bridge_line]
    assert bridge_result["functional"] is True
    assert bridge_result.get("error", None) is None
    bridge_result = bridge_results[bridge_line]
    assert bridge_result["functional"] is True
    assert bridge_result.get("error", None) is None
    bridge_result = bridge_results["obfs4 0.0.0.0:00000 BBB cert=0 iat-mode=0"]
    assert bridge_result["functional"] is True
    assert bridge_result.get("error", None) is None

    # Several bridge_lines
    response = c.get(
        "/" + config.API_PATH,
        {"bridge_lines": [
            bridge_line,
            "obfs4",
            "BBB",
        ]
        },
        content_type="application/json"
    )
    assert 200 == response.status_code
    response_dict = json.loads(response.content)
    bridge_results = response_dict["bridge_results"]
    bridge_result = bridge_results[bridge_line]
    assert bridge_result["functional"] is True
    assert bridge_result.get("error", None) is None
    bridge_result = bridge_results["obfs4"]
    assert bridge_result["functional"] is False
    assert bridge_result.get("error", None) == "bridgeline is not valid."
    bridge_result = bridge_results["BBB"]
    assert bridge_result["functional"] is False
    assert bridge_result.get("error", None) == "bridgeline is not valid."

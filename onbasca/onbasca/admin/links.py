# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

from django.urls import reverse
from django.utils.html import format_html


def relay_change_link(obj):
    return format_html(
        '<a href="{}">{}</a>'.format(
            reverse("admin:onbasca_relay_change", args=(obj.pk,)), obj
        )
    )


def relaydesc_change_link(obj):
    return format_html(
        '<a href="{}">{}</a>'.format(
            reverse("admin:onbasca_relaydesc_change", args=(obj.pk,)), obj
        )
    )


def routerstatus_change_link(obj):
    return format_html(
        '<a href="{}">{}</a>'.format(
            reverse("admin:onbasca_routerstatus_change", args=(obj.pk,)),
            obj,
        )
    )


def bwfile_change_link(obj):
    return format_html(
        '<a href="{}">{}</a>'.format(
            reverse("admin:onbasca_bwfile_change", args=(obj.pk,)), obj
        )
    )


def consensus_change_link(obj):
    return format_html(
        '<a href="{}">{}</a>'.format(
            reverse("admin:onbasca_consensus_change", args=(obj.pk,)), obj
        )
    )

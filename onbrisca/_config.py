# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause
"""Singleton to set defaults, configuration file and arguments."""
from onbasca.onbasca._config import Config as OnbascaConfig  # noqa:E402


class Config(OnbascaConfig):
    def __init__(self, defaults):
        super().__init__(defaults)

# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause
"""
Generated using factory_generator 1.0.4.
"""

import factory
from django.utils import timezone

from onbasca.onbasca.models.relaydesc import RelayDesc


class RelayDescFactoryBase(factory.django.DjangoModelFactory):
    fingerprint = factory.Faker(provider="pystr", max_chars=40)
    published = factory.Faker(
        provider="date_time_between",
        start_date="-27d",
        end_date="now",
        tzinfo=timezone.get_current_timezone(),
    )
    ed25519_master_key = factory.Faker(provider="pystr", max_chars=255)
    nickname = factory.Faker(provider="pystr", max_chars=255)
    observed_bandwidth = factory.Faker(
        provider="random_int", min=0, max=2147483647
    )
    average_bandwidth = factory.Faker(
        provider="random_int", min=0, max=2147483647
    )
    burst_bandwidth = factory.Faker(
        provider="random_int", min=0, max=2147483647
    )
    _min_bandwidth = factory.Faker(
        provider="random_int", min=0, max=2147483647
    )
    can_exit_443 = factory.Faker(
        provider="random_element", elements=(True, False)
    )
    _can_exit_443_strict = factory.Faker(
        provider="random_element", elements=(True, False)
    )
    _can_exit_v6_443 = factory.Faker(
        provider="random_element", elements=(True, False)
    )
    relay = factory.SubFactory(
        factory="onbasca.onbasca.factories.RelayFactory"
    )
    _hibernating = factory.Faker(
        provider="random_element", elements=(True, False)
    )
    overload_general = factory.Faker(
        provider="date_time_between",
        start_date="-27d",
        end_date="now",
        tzinfo=timezone.get_current_timezone(),
    )
    overload_ratelimits = factory.Faker(
        provider="date_time_between",
        start_date="-27d",
        end_date="now",
        tzinfo=timezone.get_current_timezone(),
    )
    overload_fd_exhausted = factory.Faker(
        provider="date_time_between",
        start_date="-27d",
        end_date="now",
        tzinfo=timezone.get_current_timezone(),
    )
    _flowctrl_2 = factory.Faker(
        provider="random_element", elements=(True, False)
    )
    _uptime = factory.Faker(provider="random_int", min=0, max=2147483647)

    class Meta:
        model = RelayDesc

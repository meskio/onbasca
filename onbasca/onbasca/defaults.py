# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause
import os

# paths
APP_PATH = os.path.dirname(os.path.abspath(__file__))
HOME = os.environ.get("HOME", os.path.expanduser("~"))
APP_DATA_PATH = os.path.join(HOME, ".onbasca")
TOR_DATA_PATH = os.path.join(APP_DATA_PATH, "tor")
CONFIG_PATH = os.path.join(APP_DATA_PATH, "config.toml")
BWFILE_DIR = os.path.join(APP_DATA_PATH, "bwfiles")
BWFILE_PATH = os.path.join(BWFILE_DIR, "latest.txt")
LOGS_DIR = os.path.join(APP_DATA_PATH, "logs")
SCAN_LOG_PATH = os.path.join(LOGS_DIR, "scan.log")
GENERATE_LOG_PATH = os.path.join(LOGS_DIR, "generate.log")
CLEAN_LOG_PATH = os.path.join(LOGS_DIR, "clean.log")

# For tor
EXTERNAL_CONTROL_PORT = None

TOR_CONFIG_BASE = {
    "SocksPort": "auto",
    "CookieAuthentication": "1",
    "UseMicrodescriptors": "0",
    # Get extra-info descriptors.
    # DirPort fails with tor <= 0.4.7.1-alpha-dev
    # "DownloadExtraInfo": "1",
    # "DirPort": "9030",
    # Get MaxAdvertisedBandwidth as soon as possible.
    "FetchDirInfoEarly": "1",
    # requires that you also set FetchDirInfoEarly
    "FetchDirInfoExtraEarly": "1",
    # Get descriptors even not yet in the consensus.
    "FetchUselessDescriptors": "1",
    "LearnCircuitBuildTimeout": "0",  #
}
TOR_CONFIG_DIRS = {
    # "ControlPort": "8015",
    "ControlSocket": os.path.join(TOR_DATA_PATH, "control"),
    "DataDirectory": TOR_DATA_PATH,
    "PidFile": os.path.join(TOR_DATA_PATH, "pid"),
    "Log": [
        "NOTICE file {}".format(os.path.join(TOR_DATA_PATH, "notices.log"))
    ],
}
TOR_CONFIG_CAN_FAIL = {
    # is disable to do not send extra traffic
    "ConnectionPadding": "0",
    "CircuitPadding": "0",
    # if this option is true, Tor treats every startup event as user activity,
    # and Tor will never start in Dormant mode, even if it has been unused
    # for a long time on previous runs.
    "DormantCanceledByStartup": "1",
}
TOR_CONFIG_RUNTIME = {
    "__DisablePredictedCircuits": "1",
    "__LeaveStreamsUnattached": "1",
}
TOR_CONFIG = {**TOR_CONFIG_BASE, **TOR_CONFIG_DIRS}

# For the measurements
OLDEST_DATA_DAYS = 28
NUM_THREADS = 3
FUTURE_TIMEOUT_SECS = 60

# For the bwfile
SCANNER_COUNTRY = "ZZ"
DESTINATIONS_COUNTRIES = "ZZ"
RECENT_DAYS = 5
MAX_WEIGHT_PERCENT_DIFF = 50

# Constants to check health KeyValues in the Bandwidth File
MAX_RECENT_CONSENSUS_COUNT = RECENT_DAYS * 24  # 120
MIN_HOURS_PRIORITY_LIST = 1
# As of 2020, there're less than 7000 relays.
MAX_RELAYS = 8000
# 120
MAX_RECENT_PRIORITY_LIST_COUNT = int(
    RECENT_DAYS * 24 / MIN_HOURS_PRIORITY_LIST
)
MAX_RELAYS_PER_PRIORITY_LIST = MAX_RELAYS  # 8000
# 120 * 8000 = 960000
MAX_RECENT_PRIORITY_RELAY_COUNT = (
    MAX_RECENT_PRIORITY_LIST_COUNT * MAX_RELAYS_PER_PRIORITY_LIST
)
# The following are all the same as MAX_RECENT_PRIORITY_RELAY_COUNT
# Leaving them commented for clarity.
# MAX_RECENT_MEASUREMENT_ATTEMPT_COUNT = MAX_RECENT_PRIORITY_RELAY_COUNT
# MAX_RECENT_MEASUREMENT_FAILURE_COUNT = MAX_RECENT_MEASUREMENT_ATTEMPT_COUNT
# MAX_RECENT_MEASUREMENTS_EXCLUDED_ERROR_COUNT = (
#     MAX_RECENT_MEASUREMENT_ATTEMPT_COUNT
# )
MIN_RELAYS_PER_PRIORITY_LIST = 5000


# For the Web server client
NICKNAME = "Unnamed"
# UUID = "XXX"
# TOR_VERSION = "XXX"
CERTFILE = os.path.join(
    "..", "..", APP_PATH, "tests/integration/localhost.crt"
)
WEB_SERVERS = [
    {"url": "https://localhost:28888", "verify": CERTFILE, "enabled": True}
]

DL_TIMEOUT_SECS = 10

# For bridgescan consensus
# Move these constant to onbrisca, import them in Consenus from there and add
# cli arguments.
BRIDGESCAN = True
# As of January 2023, to obtain less than 30 non_exits, this is the minimum
# uptime they have
MIN_NON_EXITS_UPTIME_DAYS = 151
MIN_NON_EXITS_COUNT = 10
MAX_NON_EXITS_COUNT = 30
NON_EXITS_FILEPATH = os.path.join(HOME, ".onbrisca", "middles.txt")
NON_EXITS_FROM_FILE = False

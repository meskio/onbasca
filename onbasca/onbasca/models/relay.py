# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause
import logging

from django.core.exceptions import ObjectDoesNotExist
from django.db import models

from onbasca.base.models.relay import RelayBase, RelayManagerBase

logger = logging.getLogger(__name__)


class RelayManager(RelayManagerBase):
    def from_router_status(self, router_status, consensus=None):
        relay = super().from_router_status(router_status, consensus)
        relay.set_dead()
        return relay


class Relay(RelayBase):
    """

    Torflow::
        dead = not ("Running" in ns.flags)

        elif kw == "opt hibernating":
          dead = True

        if not bw_observed and not dead and ("Valid" in ns.flags):
          dead = True

    """

    objects = RelayManager()
    # not used atm.
    _dead = models.BooleanField(null=True, blank=True)

    _bw_mean = models.PositiveIntegerField(null=True, blank=True)
    _bw_median = models.PositiveIntegerField(null=True, blank=True)
    _bw_filt = models.PositiveIntegerField(null=True, blank=True)

    _ratio_stream = models.FloatField(null=True, blank=True)
    _ratio_filt = models.FloatField(null=True, blank=True)
    _ratio = models.FloatField(null=True, blank=True)

    _bw_scaled = models.FloatField(null=True, blank=True)

    _measurement_latest = models.OneToOneField(
        to="Measurement",
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name="relay_related",
    )

    def is_dead(self):
        return (
            not self.routerstatus_latest()._is_running
            or self.relaydesc_latest()._hibernating
            or (
                not self.relaydesc_latest().observed_bandwidth
                and self.routerstatus_latest()._valid
            )
        )

    def set_dead(self):
        self._dead = self.is_dead()

    def set_measurement_latest(self, measurement):
        self._measurement_latest = measurement
        self.save()
        return self._measurement_latest

    def measurement_latest(self):
        try:
            return self.measurements.latest()
        except ObjectDoesNotExist:
            return None

    def measurement_latest_date(self):
        if self.measurement_latest():
            return self.measurement_latest().created_at
        return None

    def measurements(self):
        return self.measurement_set.all()

    def measurements_count(self):
        return self.measurements.count()

    def relaydesc_latest_can_exit_443(self):
        if self.relaydesc_latest():
            return self.relaydesc_latest().can_exit_443
        return None

    def routerstatus_latest_is_exit(self):
        if self.routerstatus_latest():
            return self.routerstatus_latest().is_exit
        return None

    def is_exit_can_exit_443(self):
        return (
            self.routerstatus_latest_is_exit()
            and self.relaydesc_latest_can_exit_443()
        )

    def has_2_in_flowctrl(self):
        if self.relaydesc_latest():
            return self.relaydesc_latest()._flowctrl_2
        return None

    # to generate bwfile
    @property
    def nickname(self):
        if self.routerstatus_latest():
            return self.routerstatus_latest().nickname
        elif self.relaydesc_latest():
            return self.relaydesc_latest().nickname
        return None

    @property
    def consensus_bandwidth(self):
        if self.routerstatus_latest():
            return self.routerstatus_latest().bandwidth
        return None

    @property
    def consensus_bandwidth_is_unmeasured(self):
        if self.routerstatus_latest():
            return self.routerstatus_latest().is_unmeasured
        return None

    @property
    def desc_bw_avg(self):
        if self.relaydesc_latest():
            return self.relaydesc_latest().average_bandwidth
        return None

    @property
    def desc_bw_bur(self):
        if self.relaydesc_latest():
            return self.relaydesc_latest().burst_bandwidth
        return None

    @property
    def desc_bw_obs_last(self):
        if self.relaydesc_latest():
            return self.relaydesc_latest().observed_bandwidth
        return None

    @property
    def desc_bw_obs_mean(self):
        bw = self.relaydescs().aggregate(models.Avg("observed_bandwidth"))[
            "observed_bandwidth__avg"
        ]
        return bw

    @property
    def master_key_ed25519(self):
        if self.relaydesc_latest():
            return self.relaydesc_latest().ed25519_master_key
        return None

    @property
    def success(self):
        # filter?
        return self.measurements.filter(bandwidth__isnull=False).count()

    @property
    def error_circ(self):
        # filter?
        return self.measurements.filter(error__icontains="circuit").count()

    @property
    def error_stream(self):
        return self.measurements.filter(error__icontains="urllib3").count()

    @property
    def error_second_relay(self):
        return self.measurements.filter(error__icontains="no path").count()

    @property
    def error_misc(self):
        return (
            self.measurements.filter(error__isnull=False).count()
            - self.error_circ
            - self.error_stream
            - self.error_second_relay
        )

    @property
    def measured_at(self):
        return self.measurement_latest_date()

    def min_descriptor_consensus_bandwidth(self):
        if (
            self.routerstatus_latest()
            and self.relaydescs_min_bandwidth_latest() is not None
        ):
            return min(
                self.routerstatus_latest().bandwidth,
                self.relaydescs_min_bandwidth_latest(),
            )
        return None

    def set_bw_mean(self):
        self._bw_mean = (
            self.measurements.aggregate(models.Avg("bandwidth"))[
                "bandwidth__avg"
            ]
            or 0
        )
        logger.debug("Relay bw mean: %s", self._bw_mean)
        self.save()
        return self._bw_mean

    def set_bw_median(self):
        count = (
            self.measurements.values_list("bandwidth", flat=True)
            .exclude(bandwidth__isnull=True)
            .count()
        )
        if not count:
            return None
        self._bw_median = (
            self.measurements.values_list("bandwidth", flat=True)
            .exclude(bandwidth__isnull=True)
            .order_by("bandwidth")[int(round(count / 2))]
        )
        logger.debug("Relay bw median: %s", self._bw_median)
        self.save()
        return self._bw_median

    def set_bw_filt(self):
        """

        https://gitlab.torproject.org/tpo/network-health/sbws/-/issues/40059,
        https://gitlab.torproject.org/tpo/network-health/sbws/-/issues/40080:
        Calculate bw stream and filtered without distinguishig relay types.

        """
        self._bw_filt = (
            # When there's only one measurement for every relay,
            # XXXX: why some relays return here the only measurement and others
            # not? (having both _bw_mean). To solve it: `or self._bw_mean`
            self.measurements.filter(bandwidth__gte=self._bw_mean).aggregate(
                models.Avg("bandwidth")
            )["bandwidth__avg"]
            or self._bw_mean
        )
        # logger.debug("Relay bw filt: %s", self._bw_filt)
        self.save()
        return self._bw_filt

    def set_ratio_stream(self, mu):
        if not mu:
            logger.warning("Unexpected mu %s", mu)
            return 0
        self._ratio_stream = self._bw_mean / mu
        # logger.debug("Relay stream ratio: %s", self._ratio_stream)
        self.save()
        return self._ratio_stream

    def set_ratio_filt(self, muf):
        self._ratio_filt = self._bw_filt / muf
        # logger.debug("Relay filtered stream ratio: %s", self._ratio_filt)
        self.save()
        return self._ratio_filt

    def set_ratio(self):
        # if self._ratio_stream > self._ratio_filt:
        #     logger.info("Stream ratio greater than filtered.")
        self._ratio = max(self._ratio_stream, self._ratio_filt)
        # logger.debug("Relay ratio: %s", self._ratio)
        self.save()
        return self._ratio

    def set_bw_scaled(self):
        """

        https://gitlab.torproject.org/tpo/network-health/sbws/-/issues/30274,
        https://gitlab.torproject.org/tpo/network-health/sbws/-/issues/40088,
        https://gitlab.torproject.org/tpo/network-health/sbws/-/issues/40075,
        https://gitlab.torproject.org/tpo/network-health/sbws/-/issues/40014,
        https://gitlab.torproject.org/tpo/network-health/sbws/-/issues/40091:
        solved by using relay descriptors' bandwidth but no consensus weight.

        """
        # XXXX: version 3:
        # - set the consensus bw if no measurement
        # - set the measured bw if no descriptors bw
        if self.set_relaydescs_min_bandwidth_mean() is not None:
            self._bw_scaled = self._ratio * self._relaydescs_min_bandwidth_mean
            logger.debug("Relay scaled bandwidth: %s", self._bw_scaled)
        else:
            logger.info(
                "Scaled bandwidth is 0 for relay %s because it has not "
                "descriptors' minimum bandwidth.",
                self.fingerprint,
            )
            self._bw_scaled = 0
        self.save()
        return self._bw_scaled

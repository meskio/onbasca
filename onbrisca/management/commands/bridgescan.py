# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

"""Measure bridges' bandwidth."""
import logging

from onbrisca import config
from onbrisca.models.bridge_scanner import BridgeScanner

from onbasca.onbasca import util
from onbasca.onbasca.management.commands.common_cmd import OnbascaCommand

logger = logging.getLogger(__name__)


class Command(OnbascaCommand):
    help = __doc__

    def add_arguments(self, parser):
        super().add_arguments(config, parser)
        parser.add_argument(
            "-p",
            "--external-control-port",
            required=False,
            type=int,
            help="External tor control port to connect to. Useful for tests.",
        )

    def handle(self, *args, **options):
        super().handle(config, *args, **options)
        util.modify_logging(config, log_level=options.get("log_level", None))
        scanner = BridgeScanner.load()
        scanner.init(port=config.EXTERNAL_CONTROL_PORT)
        scanner.run()

# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

from django.contrib import admin

from onbasca.onbasca.models.scanner import Scanner


@admin.register(Scanner)
class ScannerAdmin(admin.ModelAdmin):
    list_display = (
        "_obj_created_at",
        "started_at",
        "heartbeat",
        "elapsed_time",
        "nickname",
        "uuid",
    )

#!/bin/bash
# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: CC0-1.0
# Quick and dirty script to uninstall `onbrisca` in Debian
# WARNING: this may disable and uninstall services that you still need
set -e

echo "This script has to be run as root user"
export LANG=en_US.UTF-8
USER=${USER-onbasca}
POSTGRES_DB=${POSTGRES_DB-onbasca}
POSTGRES_USER=${POSTGRES_USER-onbasca}
POSTGRES_PASSWORD=${POSTGRES_PASSWORD-onbasca}

echo "Removing postgres database and user"
su -c "dropdb $POSTGRES_DB" postgres
su -c "dropuser $POSTGRES_USER" postgres
pg_ctlcluster 13 main stop

echo "Removing local user"
userdel -r "$USER"

echo "Removing some system dependencies"
#apt purge curl tor systemd postgresql postgresql-contrib

echo "Removing python system dependencies"
apt purge python3-stem python3-aiohttp-socks python3-requests \
    python3-toml python3-colorlog \
    python3-sqlparse python3-asgiref python3-socks \
    python3-psycopg2 python3-gunicorn
# These will be no longer required
# python3-aiohttp python3-async-timeout python3-attr python3-certifi python3-chardet python3-idna python3-multidict python3-typing-extensions python3-urllib3
#  python3-yarl
apt autoremove

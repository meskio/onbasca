.. SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
..
.. SPDX-License-Identifier: CC0-1.0

Glossary
==========

.. glossary::

   directory authority
        a special-purpose relay that maintains a list of currently-running
        relays and periodically publishes a consensus together with the other
        directory authorities.

        .. seealso::

           [MetricsGlossary]_

   bandwidth authority
        A :term:`directory authority` that runs a :term:`scanner` and a
        :term:`generator` or obtain :term:`Bandwidth File` s from a
        :term:`generator`.

   scanner
        Term to refer to the process that measures the relays' bandwidth.
        It is also called :term:`generator` when it is the same tool that is
        used to generate :term:`Bandwidth File` s.

   generator
        Term to refer to the tool that generates the
        :term:`Bandwidth File` s. Often used as a synonym for
        :term:`scanner`.

   Bandwidth File
        The file generated by :term:`generator` s that is read by the
        :term:`directory authority` s and included in their votes.

        .. seealso::

           [BandwidthFile]_

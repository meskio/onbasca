# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause
import platform

from requests import __version__ as requests_version
from stem import __version__ as stem_version

from onbasca import __version__
from onbasca.base.constants import *  # noqa: F401, F403

# Units
KIB = 1024
GIB = 1073741824

# For the measurements
# Not used atm.
MIN_DL_BYTES = 1
MAX_DL_BYTES = GIB
INITIAL_DL_BYTES = 16 * KIB
DL_TOOFAST = 1
MIN_DL_SECS = 5
# DL_SECS = 11
DL_SECS = 5
DL_TARGET = 6
MAX_DL_SECS = 10

# For the Web server client
USER_AGENT_PART = "onbasca/{} ({}) " "Python/{} Requests/{} Stem/{}".format(
    __version__,
    platform.platform(),
    platform.python_version(),
    requests_version,
    stem_version,
)
HTTP_HEADERS = {
    "Tor-Bandwidth-Scanner-Nickname": "{}",
    "Tor-Bandwidth-Scanner-UUID": "{}",
    "User-Agent": USER_AGENT_PART + " Tor/{}",
    "Connection": "keep-alive",
}
HTTP_GET_HEADERS = {
    # 'Range': '{}',
    "Accept-Encoding": "identity",
    "Connection": "close",
}

# For the bwfile
MIN_PERCT_RELAYS_REPORT = 60

DESC_OVERLOAD_KEY_ATTRS = {
    "overload-general": "overload_general",
    "overload-ratelimits": "overload_ratelimits",
    "overload-fd-exhausted": "overload_fd_exhausted",
}

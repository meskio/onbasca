.. SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
..
.. SPDX-License-Identifier: CC0-1.0

.. _diffsbws:

Differences with sbws
=====================

1. External API:

   1.1. Bandwidth File

   - onbasca takes the observed bandwidth mean instead of the last one
     (solves `tpo/network-health/sbws#40083
     <https://gitlab.torproject.org/tpo/network-health/sbws/-/issues/40083>`_).

   - onbasca scales the bandwidth using the last descriptor, not the one
     available while measuring (solves `tpo/network-health/sbws#40081
     <https://gitlab.torproject.org/tpo/network-health/sbws/-/issues/40081>`_).

   1.2. Scanner

   - onbasca only measures relays with a descriptor (solves
     `tpo/network-health/sbws#40076
     <https://gitlab.torproject.org/tpo/network-health/sbws/-/issues/40076>`_).
   - onbasca does not try to download the data 3 times adjusting the size
     according to the time it took, as sbws does (see `#2
     <https://gitlab.torproject.org/tpo/network-health/onbasca/-/issues/2>`_,
     `#3 <https://gitlab.torproject.org/tpo/network-health/sbws/-/issues/3>`_).

     In the future it should use a better algorithm than sbws (see
     `tpo/network-health/sbws#30232
     <https://gitlab.torproject.org/tpo/network-health/sbws/-/issues/30232>`_,
     `tpo/network-health/sbws#29291
     <https://gitlab.torproject.org/tpo/network-health/sbws/-/issues/29291>`_).
   - onbasca does not try to measure as entry an exit that fails to be
     measured as exit (solved in sbws by `#40041
     <https://gitlab.torproject.org/tpo/network-health/sbws/-/issues/40041>`_
     and others). Maybe this should be re-implemented.

   1.3. Logs

   - onbasca rotates logs by days, not bytes, as sbws does
     (solves `tpo/network-health/sbws#40066
     <https://gitlab.torproject.org/tpo/network-health/sbws/-/issues/40066>`_).

2. Behaviour and/or trend days after an incident:

   2.1. Web Server

   - onbasca does not enable/disable the Web Server (`#18
     <https://gitlab.torproject.org/tpo/network-health/onbasca/-/issues/18>`_).

     In the future it should use a better algorithm than sbws, cause it
     was prone to bugs (see `tpo/network-health/sbws#40062
     <https://gitlab.torproject.org/tpo/network-health/sbws/-/issues/40062>`_,
     `tpo/network-health/sbws#40067
     <https://gitlab.torproject.org/tpo/network-health/sbws/-/issues/40067>`_).

3. Tests

   - onbasca assumes congestion control enabled (`cc_alg=2`, `bwscanner_cc>=1`)
     while sbws only enable it for congestion control tests. The reason is
     due sbws needing to be compatible with previous tests.

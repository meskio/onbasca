.. SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
..
.. SPDX-License-Identifier: CC0-1.0

.. _install:

Installation
============

System dependencies
--------------------

The following software must be installed:

- [tor]_ (last stable version is recommended, see [CoreTorReleases]_)
  To use only exits that implement congestion control, tor >= 0.4.7.4-alpha-dev
- [PostgreSQL]_ ([libpq-dev]_)
- [Python3]_ (>= 3.7)

Additionally, a PostgreSQL database is needed
(see :ref:`Create-the-database`) and a system user with HOME to store the
configuration, logs and other data.

.. note:: You can find scripts as an example to install and run ``onbrisca``
   in Debian at  `<../../../deploy_onbrisca>`_ (in the local directory or Tor Project
   Gitlab). This section corresponds to
   `<../../../deploy_onbrisca/install_root.sh>`_ .


Python dependencies
--------------------

The version of the following dependencies might be unupdated.
The list of updated versions is in the [setup.cfg]_ file.

- [aiohttp]_ >= 3.7, only in the case of installing ``onbrisca``
- [aiohttp-socks]_ >= 0.5, only in the case of installing ``onbrisca``
- [colorlog]_ >= 4.7
- [Django3]_ or [Django_4.1]_ only in the case of installing ``onbrisca``
- [gunicorn]_ >= 2.1, only in the case of installing ``onbrisca``
- [Psycopg2]_ >= 2.8
- [stem]_ >= 1.8.0
- [requests]_ (with [socks]_ support) >= 2.10.0
- [toml]_ >= 0.10

It is recommend to install the dependencies from your system package manager.

As of February 2023, the following dependencies can be install from Debian
bullseye by an user with root permissions::

    apt install python3-stem python3-aiohttp-socks python3-psycopg2 \
    python3-requests python3-toml python3-virtualenv

[Django_4.1]_ is not available in Debian stable, as well as the versions of
two of its dependencies: ``asigref`` and ``python-socks``.

.. seealso:: `<../../../deploy_onbrisca/install_root.sh>`_ .

Installation from source
------------------------

At the moment, there're no system packages for ``onbasca`` and it must be
install from source.

A system user with HOME can install ``onbasca`` with the following steps:

Clone ``onbasca``::

    git clone https://gitlab.torproject.org/tpo/network-health/onbasca.git
    git checkout $(git describe --abbrev=0 --tags)

(``git describe --abbrev=0 --tags`` provides the latest stable version which
should be used in production)

If you're installing ``onbasca`` for development or testing, it's recommend to
install all the dependencies and packages in a [virtualenv]_.

To create a virtualenv using the already installed system packages::

    virtualenv --system-site-packages <virtualenvpath>
    source <virtualenvpath>

After creating and activating the virtualenv you can install the dependencies
with the command::

    cd onbasca
    python3 setup.py install

If the virtualenv was created with ``--system-site-packages`` option, only
[Django_4.1]_, ``python-socks`` and ``asgiref`` (>=3.6) should be install
from [PyPI]_.

.. note:: `In Debian, ``python3-python-sock`` is a different package than
   ``python3-socks``.

.. seealso:: `<../../../deploy_onbrisca/install_onbrisca.sh>`_ .

.. _create-the-database:

Create the database
-------------------

In a Debian system with root permissions::

    su postgres
    psql
    create user <username> with password '<password>';
    create database <dbname>;
    grant all privileges on database <dbname> to <username>;

.. seealso:: `<../../../deploy_onbrisca/install_root.sh>`_ .

Database configuration and initialization
-----------------------------------------

After having a database for ``onbasca``, set the credentials in either
``onbascapr/settings/local.py`` (if installing the relay bandwidth scanner) or
``onbriscapr/settings/local.py`` (if installing the bridge bandwidth scanner).

There's an example file that you can copy and customize at
`<../../../deploy_onbrisca/local.py>`_.

After that, create the database schema for ``onbasca`` running::

    ./manage.py makemigrations onbasca

Or for ``onbrisca`` running::

    ./manage_onbrisca.py makemigrations onbasca
    ./manage_onbrisca.py makemigrations onbrisca

Then create the tables::

    ./manage.py migrate

.. seealso:: `<../../../deploy_onbrisca/install_onbrisca.sh>`_ .

Configuration
-------------

A ``config.toml`` is needed.
In the case of ``onbasca`` place it in $HOME/.onbasca/.
In the case of ``onbrisca`` place it in $HOME/.onbrisca/.

For ``onbrisca``, there is an example file at
`<../../../deploy_onbrisca/config.toml>`_ .
For ``onbasca``, see :ref:`deploy`.

Run the scanner and the generator for development
-------------------------------------------------

To run the scanner and the generator::

    ./manage.py scan
    ./manage.py generate

or::

    ./scan.py
    ./generate.py

Run the bridge scanner for development
--------------------------------------

To run the bridge scanner::

    ./manage_onbrisca bridgescan

or::

    ./bridgescan.py

To run the development Web API server::

    ./manage_onbrisca runserver

Run the scanner and the generator for production
-------------------------------------------------

See `<DEPLOY.rst>`_ (in the local directory or Tor Project Gitlab)
or `<DEPLOY.html>`_ (local build).

Run the bridge scanner for production
--------------------------------------

It's recommended to create systemd units to run both the scanner and the Web
API server.
There're example systemd files at `<../../../deploy_onbrisca/systemd/user>`_
and how to launch them at `<../../../deploy_onbrisca/start.sh>`_ .

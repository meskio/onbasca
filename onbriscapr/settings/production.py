# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause
"""Django production settings for onbriscapr project."""
from .base import *  # noqa: 403

DEBUG = False
ALLOWED_HOSTS = ["localhost"]
# For the HTTP endpoint security

# At the moment, there is no need for HTTPS
# MIDDLEWARE = [
#     "django.middleware.security.SecurityMiddleware",
#     "django.middleware.csrf.CsrfViewMiddleware",
#     "django.middleware.clickjacking.XFrameOptionsMiddleware",
# ]

# # HTTPS settings
# SESSION_COOKIE_SECURE = True
# CSRF_COOKIE_SECURE = True
# SECURE_SSL_REDIRECT = True

# # HSTS settings
# SECURE_HSTS_SECONDS = 31536000 # 1 year
# SECURE_HSTS_PRELOAD = True
# SECURE_HSTS_INCLUDE_SUBDOMAINS = True

try:
    from .local import *
except ImportError:
    print("Missing local.py in settings.")

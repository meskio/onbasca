.. SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
..
.. SPDX-License-Identifier: CC0-1.0

.. onbasca documentation master file, created by
   sphinx-quickstart on Thu Mar 18 18:36:24 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to onbasca's documentation!
===================================

User main documentation
------------------------

Included in the [root_directory]_ :

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   README
   INSTALL
   DEPLOY
   config.toml

Developer/technical documentation
----------------------------------

Included in the [docs_directory]_ :

.. toctree::
   :maxdepth: 1

   history
   differences_sbws
   diagrams
   modules
   glossary
   references

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

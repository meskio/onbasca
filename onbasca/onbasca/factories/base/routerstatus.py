# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause
"""
Generated using factory_generator 1.0.4.
"""

import factory
from django.utils import timezone

from onbasca.onbasca.models.routerstatus import RouterStatus


class RouterStatusFactoryBase(factory.django.DjangoModelFactory):
    fingerprint = factory.Faker(provider="pystr", max_chars=40)
    published = factory.Faker(
        provider="date_time_between",
        start_date="-27d",
        end_date="now",
        tzinfo=timezone.get_current_timezone(),
    )
    nickname = factory.Faker(provider="pystr", max_chars=19)
    address = factory.Faker(provider="pystr", max_chars=255)
    bandwidth = factory.Faker(provider="random_int", min=0, max=2147483647)
    is_unmeasured = factory.Faker(
        provider="random_element", elements=(True, False)
    )
    is_exit = factory.Faker(provider="random_element", elements=(True, False))
    relay = factory.SubFactory(
        factory="onbasca.onbasca.factories.RelayFactory"
    )
    consensus = factory.SubFactory(
        factory="onbasca.onbasca.factories.ConsensusFactory"
    )
    _exit = factory.Faker(provider="random_element", elements=(True, False))
    _guard = factory.Faker(provider="random_element", elements=(True, False))
    _guard_exit = factory.Faker(
        provider="random_element", elements=(True, False)
    )
    _middle = factory.Faker(provider="random_element", elements=(True, False))
    _authority = factory.Faker(
        provider="random_element", elements=(True, False)
    )
    _running = factory.Faker(provider="random_element", elements=(True, False))
    _valid = factory.Faker(provider="random_element", elements=(True, False))
    _measurement_latest = factory.SubFactory(
        factory="onbasca.onbasca.factories.MeasurementFactory"
    )
    _stable = factory.Faker(provider="random_element", elements=(True, False))
    _fast = factory.Faker(provider="random_element", elements=(True, False))

    class Meta:
        model = RouterStatus

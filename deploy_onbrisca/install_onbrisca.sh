#/bin/bash
# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: CC0-1.0
set -e

export LANG=en_US.UTF-8
USER=${USER-onbasca}

echo "Run this script as the user $USER"

echo "Changing to user HOME directory"
cd "$HOME"

echo "Install package"
virtualenv --system-site-packages .env
source .env/bin/activate
cd onbasca
python setup.py develop
# This ^ will also install from pypi:
# python_socks-2.1.1-py3-none-any.whl
# Django-4.2a1-py3-none-any.whl
# asgiref-3.6.0-py3-none-any.whl

echo "Install systemd user units"
echo "Creating run dir for the services socket file"
mkdir -p "$HOME"/.config/systemd/user/
cp deploy_onbrisca/systemd/user/* "$HOME"/.config/systemd/user/
mkdir -p "$HOME"/log

echo "Copy and change onbriscapr/settings/local.py"
cp deploy_onbrisca/local.py "$HOME"/onbasca/onbriscapr/settings/

echo "Copying $HOME/.onbrisca/config.toml"
mkdir -p "$HOME"/.onbrisca
cp ./deploy_onbrisca/config.toml  "$HOME"/.onbrisca/config.toml

echo "Initializing database"
./manage_onbrisca.py makemigrations onbasca
./manage_onbrisca.py makemigrations onbrisca
./manage_onbrisca.py migrate

.. SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
..
.. SPDX-License-Identifier: CC0-1.0

onbasca - config.toml (5)
=========================

DESCRIPTION
-----------

onbasca configuration file.

**onbasca** (1) ``scan`` command requires a configuration file with the
`[default]`` section including ``destinations_countries`` and
``scanner_country`` .

There must be at least one ``[[default.WEB_SERVERS]]`` subsection.

SECTIONS
--------

default

  scanner_country = STR
    ISO 3166-1 alpha-2 country code where the scanner is running.

  destinations_countries  = STR
    One or more ISO 3166-1 alpha-2 country code where the Web server(s) arg
    running.
    Use ZZ if the Web server URL is a domain name and it is in a CDN.

  WEB_SERVERS

    It is required to set at least one Web server to run the scanner.
    It is recommended to set several Web servers so that the scanner can
    continue if one fails.

    url = STR
      The URL to the destination. It must include a file path.
      It can use both http or https.
      (Default: https://localhost:28888

    verify = STR
      It can be either BOOL, hether or not to verify the Web server certificate
      or the file path with the server self-signed certificate.
      (Default: True)

    enabled = BOOL
      Whether or not the Web server is enabled.
      (Default: True)


EXAMPLE
--------

Minimal configuration example::

  [default]
  destinations_countries = "ZZ"
  scanner_country = "ZZ"

  [[default.WEB_SERVERS]]
  url = "https://localhost:28888"
  verify = "tests/integration/localhost.crt"
  enabled = true

FILES
-----

$HOME/.onbasca/config.toml
   Default ``onbasca`` user configuration path.

/etc/onbasca/config.toml
  Default ``onbasca`` Debian configuration path.

Any other path to the configuration file can be specified using the
``onbasca`` argument ``-c``

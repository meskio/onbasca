# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause
# local (or production or development) in that order will import the others.
try:
    from .local import *
except ImportError as e:
    print("import error", e)
    try:
        from .production import *
    except ImportError:
        from .development import *

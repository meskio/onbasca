# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

import logging

from django.db import models
from django.db.models.query import sync_to_async

from onbasca.base.models import BaseModel
from onbasca.onbasca import constants
from onbasca.onbasca.models.webserver import WebServer

from .bridge import Bridge

logger = logging.getLogger(__name__)


class BridgeMeasurement(BaseModel):
    class Meta:
        get_latest_by = "_obj_created_at"

    bridge = models.ForeignKey(
        Bridge,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        related_name="measurements",
    )
    webserver = models.ForeignKey(
        WebServer, on_delete=models.CASCADE, blank=True, null=True
    )
    bandwidth = models.PositiveIntegerField(blank=True, null=True)
    error = models.TextField(blank=True, null=True)

    def __str__(self):
        return "{}, {}".format(self.bridge, self.error or str(self.bandwidth))

    async def asave(self, *args, **kwargs):
        await sync_to_async(super().save)(*args, **kwargs)

    async def finish_with_error(self, error):
        self.error = error
        logger.info("Finished measurement %s", self)
        await self.asave()

    async def ameasure_bandwidth(
        self, http_client, bytes_range, size=constants.INITIAL_DL_BYTES
    ):
        logger.debug(
            "Measuring bw with web server %s, bytes range %s " "and size %s.",
            self.webserver.url,
            bytes_range,
            size,
        )
        response = await http_client.aget(self.webserver.url)
        if isinstance(response, Exception) or isinstance(response, str) or response.status != 200:
            await self.finish_with_error(response)
            return response
        self.bandwidth = size / response.elapsed_time
        await self.asave()
        logger.info("Finished measurement %s.", self)
        logger.debug(
            "Measured bandwidth: %s Bytes/seconds",
            self.bandwidth,
        )
        return self.bandwidth

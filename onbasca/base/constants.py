# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

DATETIME_FORMAT = "%Y-%m-%dT%H:%M:%S"
KB = 1000
TERMINATOR = "===="

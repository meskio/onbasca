.. SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
..
.. SPDX-License-Identifier: CC0-1.0

.. _deploy:

Deployment
==========

Hardware requirements
---------------------

- A machine to run the ``onbasca`` :term:`scanner` and the :term:`generator`
  or the ``onbrisca`` bridge scanner.
- The same or other machine to run one or more Web servers(s) that serve a
  large file.

.. _webserver_requirements:

Web server requirements
-----------------------

- A Web server installed and running that supports HTTP ``GET``, ``HEAD`` and
  ``Range`` (:rfc:`7233`) requests.
  [Apache]_ HTTP Server and [nginx]_ support them.

  .. important::
     Note that if the server is configured with ``keep-alive`` timeout, it'd need
     to be at least the same timeout as in the onbasca HTTP requests, which is 10
     seconds by default (``http_timeout`` variable in the configuration file, see
     more about in the next section).
- TLS support to avoid HTTP content caches at the various exit nodes.
- Certificates can be self-signed.
- A large file; at the time of writing, at least 1 GiB in size
  It can be created running::

      head -c $((1024*1024*1024)) /dev/urandom > 1GiB

- A fixed IP address or a domain name.
- Bandwidth: at least 12.5MB/s (100 Mbit/s).
- Network traffic: around 12-15GB/day.

If you want, use a [ContentDeliveryNetwork]_ (CDN) in order to make the
destination IP closer to the scanner exit.

onbasca scanner and generator or onbrisca scanner setup
-------------------------------------------------------

.. important:: To facilitate debugging, it is recommended that the system timezone
   is set to UTC.

To set the timezone to UTC in Debian::

  apt-get --reinstall install tzdata
  ln -sf /usr/share/zoneinfo/UTC /etc/localtime
  update-initramfs -u

Install ``onbasca`` or ``onbrisca`` according to `<INSTALL.rst>`_ (in the local
directory or Tor Project Gitlab) or `<INSTALL.html>`_
(local build or [online_documentation]_).

scanner and generator configuration for production
--------------------------------------------------

It is **needed** to create a configuration file with at least one Web server.

It is recommended to set several Web servers so that the ``onbasca`` or
``onbrisca`` ``scanner`` can continue if one fails.

If ``onbasca`` is installed from the sources as a non-root user then create the
configuration file in ``~/.onbasca/config.toml``.
In the case of ``onbrisca``, create the configuration file in
``~/.onbrisca/config.toml``.

You can see an example with a minimal configuration for ``onbasca`` here:

.. literalinclude:: config.example.toml
    :caption: Example config.example.toml

More details about the configuration file can be found in
``./docs/source/config.toml.rst`` (in the local directory or Tor Project
Gitlab) or `<config.toml.html>`_  (local build or [online_documentation]_.

onbrisca scanner and endpoint deployment for production
-------------------------------------------------------

You can find scripts as an example to install and run ``onbrisca`` in Debian
at  `<../../../deploy_onbrisca>`_ (in the local directory or Tor Project Gitlab)

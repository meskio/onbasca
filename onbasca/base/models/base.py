# SPDX-FileCopyrightText: 2022 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

from django.db import models


class BaseModel(models.Model):
    class Meta:
        abstract = True

    _obj_created_at = models.DateTimeField(auto_now_add=True, null=True)
    _obj_updated_at = models.DateTimeField(auto_now=True, null=True)
